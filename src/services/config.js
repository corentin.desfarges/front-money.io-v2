const isProd = process.env.NODE_ENV === 'production';

export default {
  host: `https://${isProd ? 'api' : 'dev'}.corentindesfarges.fr`
};
