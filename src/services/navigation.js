import * as P from "bluebird";

export const changeTab = (path) => dispatch =>
  P.resolve()
    .then(() => dispatch({type: 'CHANGE_TAB', path}));